/*
  Purpose: Check the size of variables
  Description: To get the exact size of a type or a variable on a particular platform,
  you can use the sizeof method. The expression sizeof(type) yields the storage size of
  the object or type in bytes
*/

using System;
namespace DataTypeApplication
{
  class Program //always Capital case
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Size of int = {0}", sizeof(int)); // = 4
      Console.WriteLine("Size of short = {0}", sizeof(short)); // = 2
      Console.WriteLine("Size of long = {0}", sizeof(long));  // = 8
      Console.WriteLine("Size of float = {0}", sizeof(float)); // = 4
      Console.WriteLine("Size of double = {0}", sizeof(double)); // = 8
      Console.WriteLine("Size of byte = {0}", sizeof(byte)); // = 1
      Console.WriteLine("Size of char = {0}", sizeof(char));  // = 2
      Console.WriteLine("Size of bool = {0}", sizeof(bool));  // = 1

    }
  }
}
