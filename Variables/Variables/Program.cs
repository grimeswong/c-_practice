﻿using System;

namespace Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var name = "Grimes Wong"; //use you name here
            Console.WriteLine($"Hello {name}!", name);
        }
    }
}