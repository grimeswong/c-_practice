﻿using System;

namespace RectangleApplication
{
    class Rectangle
    {
        // member variables
        double length;
        double width;

        // member function
        public void Acceptdetails()
        {
            length = 4.5;
            width = 3.5;
        }

        // member function
        public double GetArea()
        {
            return length * width;
        }

        public void Display()
        {
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("Width: {0}", width);
            Console.WriteLine("Area: {0}", GetArea());
        }  
    }
    class ExecuteRetangle
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle();  // Instantiates the Rectangle class
            r.Acceptdetails();
            r.Display();
            Console.ReadLine();
        }
    }

}