using System;   // Like import, using keyword is used to includes System namespace in the program

/* Namespace Declaration (A collection of classes) that avoid the naming confusion*/
namespace HelloworldApplication // Application name which can have many classes
{
    public class Helloworld    /* A Class */
    {
        /* Class Main Method (The entry point of all C# program) */
        public static void Main(string[] args)
        {
            /* my first program in C# */
            Console.WriteLine("Hello World!");
            Console.ReadKey();      //Read the key enter
            //This makes the program wait for a key press and it prevents the screen from running and closing quickly when the program is launched from Visual Studio.NET.
        }
    }   
}

